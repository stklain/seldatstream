USE [master]
GO
/****** Object:  Database [ClothingStore]    Script Date: 03/09/2017 10:20:17 ******/
CREATE DATABASE [ClothingStore]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'ClothingStore', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\DATA\ClothingStore.mdf' , SIZE = 5120KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'ClothingStore_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\DATA\ClothingStore_log.ldf' , SIZE = 2048KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [ClothingStore] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [ClothingStore].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [ClothingStore] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [ClothingStore] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [ClothingStore] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [ClothingStore] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [ClothingStore] SET ARITHABORT OFF 
GO
ALTER DATABASE [ClothingStore] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [ClothingStore] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [ClothingStore] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [ClothingStore] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [ClothingStore] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [ClothingStore] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [ClothingStore] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [ClothingStore] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [ClothingStore] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [ClothingStore] SET  DISABLE_BROKER 
GO
ALTER DATABASE [ClothingStore] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [ClothingStore] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [ClothingStore] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [ClothingStore] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [ClothingStore] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [ClothingStore] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [ClothingStore] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [ClothingStore] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [ClothingStore] SET  MULTI_USER 
GO
ALTER DATABASE [ClothingStore] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [ClothingStore] SET DB_CHAINING OFF 
GO
ALTER DATABASE [ClothingStore] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [ClothingStore] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [ClothingStore] SET DELAYED_DURABILITY = DISABLED 
GO
USE [ClothingStore]
GO
/****** Object:  Table [dbo].[Admin]    Script Date: 03/09/2017 10:20:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Admin](
	[AdminID] [int] IDENTITY(1,1) NOT NULL,
	[AdminName] [nvarchar](50) NOT NULL,
	[AdminPassword] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_Admin] PRIMARY KEY CLUSTERED 
(
	[AdminID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Categories]    Script Date: 03/09/2017 10:20:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Categories](
	[CategoryID] [int] IDENTITY(1,1) NOT NULL,
	[CategoryName] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_Categories] PRIMARY KEY CLUSTERED 
(
	[CategoryID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Clothes]    Script Date: 03/09/2017 10:20:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Clothes](
	[ClothID] [int] IDENTITY(1,1) NOT NULL,
	[CategoryID] [int] NOT NULL,
	[TypeID] [int] NOT NULL,
	[CompanyID] [int] NOT NULL,
	[Price] [money] NOT NULL,
	[Discount] [real] NULL CONSTRAINT [DF_Clothes_Discount]  DEFAULT ((0)),
	[Image] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_Clothes_1] PRIMARY KEY CLUSTERED 
(
	[ClothID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Companies]    Script Date: 03/09/2017 10:20:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Companies](
	[CompanyID] [int] IDENTITY(1,1) NOT NULL,
	[CompanyName] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_Companies] PRIMARY KEY CLUSTERED 
(
	[CompanyID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Types]    Script Date: 03/09/2017 10:20:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Types](
	[TypeID] [int] IDENTITY(1,1) NOT NULL,
	[TypeName] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_Types] PRIMARY KEY CLUSTERED 
(
	[TypeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET IDENTITY_INSERT [dbo].[Admin] ON 

INSERT [dbo].[Admin] ([AdminID], [AdminName], [AdminPassword]) VALUES (3, N'a', N'1')
SET IDENTITY_INSERT [dbo].[Admin] OFF
SET IDENTITY_INSERT [dbo].[Categories] ON 

INSERT [dbo].[Categories] ([CategoryID], [CategoryName]) VALUES (1, N'נשים')
INSERT [dbo].[Categories] ([CategoryID], [CategoryName]) VALUES (2, N'גברים')
INSERT [dbo].[Categories] ([CategoryID], [CategoryName]) VALUES (3, N'ילדים')
INSERT [dbo].[Categories] ([CategoryID], [CategoryName]) VALUES (4, N'תינוקות')
SET IDENTITY_INSERT [dbo].[Categories] OFF
SET IDENTITY_INSERT [dbo].[Clothes] ON 

INSERT [dbo].[Clothes] ([ClothID], [CategoryID], [TypeID], [CompanyID], [Price], [Discount], [Image]) VALUES (1, 4, 1, 2, 50.0000, 2, N'cef3569b-d1a5-4374-9b0b-b2ed260d7d3f.jpg')
INSERT [dbo].[Clothes] ([ClothID], [CategoryID], [TypeID], [CompanyID], [Price], [Discount], [Image]) VALUES (2, 2, 5, 2, 455.0000, 5, N'd56dde2a-39b7-4925-9d49-7f9c7c3f81f0.jpeg')
INSERT [dbo].[Clothes] ([ClothID], [CategoryID], [TypeID], [CompanyID], [Price], [Discount], [Image]) VALUES (3, 3, 1, 1, 150.0000, 10, N'33ad7bc4-7426-4224-a22f-f9768542269c.jpeg')
INSERT [dbo].[Clothes] ([ClothID], [CategoryID], [TypeID], [CompanyID], [Price], [Discount], [Image]) VALUES (4, 3, 4, 1, 170.0000, 15, N'27777e5d-e25d-4891-8501-c6364173d3bf.jpeg')
INSERT [dbo].[Clothes] ([ClothID], [CategoryID], [TypeID], [CompanyID], [Price], [Discount], [Image]) VALUES (5, 3, 1, 1, 135.0000, 5, N'9d920c8a-1804-4dd2-811d-3dcc8d428c26.jpg')
INSERT [dbo].[Clothes] ([ClothID], [CategoryID], [TypeID], [CompanyID], [Price], [Discount], [Image]) VALUES (6, 3, 1, 1, 100.0000, 15, N'fef84e4f-a4c9-43a1-b2ab-649b638d1a1b.jpg')
INSERT [dbo].[Clothes] ([ClothID], [CategoryID], [TypeID], [CompanyID], [Price], [Discount], [Image]) VALUES (7, 3, 5, 1, 150.0000, 5, N'bd950b1d-c861-4c25-9348-62b635c202a9.jpeg')
INSERT [dbo].[Clothes] ([ClothID], [CategoryID], [TypeID], [CompanyID], [Price], [Discount], [Image]) VALUES (8, 3, 1, 1, 167.0000, 7, N'71a73d76-de52-4a44-86bf-1a6578cc517c.jpeg')
INSERT [dbo].[Clothes] ([ClothID], [CategoryID], [TypeID], [CompanyID], [Price], [Discount], [Image]) VALUES (9, 3, 5, 1, 149.0000, 5, N'8d403574-ea30-494d-8c25-5cfc029d674d.jpg')
INSERT [dbo].[Clothes] ([ClothID], [CategoryID], [TypeID], [CompanyID], [Price], [Discount], [Image]) VALUES (10, 3, 1, 2, 300.0000, 50, N'3c3da0b5-1d53-4535-819e-a094df621e42.jpeg')
INSERT [dbo].[Clothes] ([ClothID], [CategoryID], [TypeID], [CompanyID], [Price], [Discount], [Image]) VALUES (11, 4, 1, 1, 250.0000, 20, N'7f90fcdb-b7cb-4a5c-8c11-460a9c2783e7.jpg')
INSERT [dbo].[Clothes] ([ClothID], [CategoryID], [TypeID], [CompanyID], [Price], [Discount], [Image]) VALUES (12, 4, 1, 1, 450.0000, 75, N'1eeb54c0-fd93-47b6-820f-9619ae50b086.jpg')
INSERT [dbo].[Clothes] ([ClothID], [CategoryID], [TypeID], [CompanyID], [Price], [Discount], [Image]) VALUES (13, 4, 6, 2, 222.0000, 2, N'8c1243d3-fd32-4ec0-8236-b3af25a89f94.jpg')
INSERT [dbo].[Clothes] ([ClothID], [CategoryID], [TypeID], [CompanyID], [Price], [Discount], [Image]) VALUES (14, 1, 6, 1, 350.0000, 15, N'9dedccfd-f3c3-47ef-ad8f-d23d5c6d5b60.jpeg')
INSERT [dbo].[Clothes] ([ClothID], [CategoryID], [TypeID], [CompanyID], [Price], [Discount], [Image]) VALUES (17, 1, 4, 3, 670.0000, 40, N'a802a17f-a23e-4ec1-ae84-6e621798fb31.jpg')
INSERT [dbo].[Clothes] ([ClothID], [CategoryID], [TypeID], [CompanyID], [Price], [Discount], [Image]) VALUES (19, 1, 4, 3, 890.0000, 20, N'b8da6f4a-71e7-4cb2-b77f-2903a9b0db4e.jpg')
INSERT [dbo].[Clothes] ([ClothID], [CategoryID], [TypeID], [CompanyID], [Price], [Discount], [Image]) VALUES (20, 1, 1, 5, 450.0000, 50, N'c08c8b46-8bce-43a6-a18a-7879c646be53.jpg')
INSERT [dbo].[Clothes] ([ClothID], [CategoryID], [TypeID], [CompanyID], [Price], [Discount], [Image]) VALUES (21, 1, 12, 6, 150.0000, 5, N'59a7ebfd-3ff8-4413-8b38-871dcc4d37fb.jpg')
INSERT [dbo].[Clothes] ([ClothID], [CategoryID], [TypeID], [CompanyID], [Price], [Discount], [Image]) VALUES (22, 1, 15, 5, 180.0000, 15, N'f020d010-26db-4dee-9e50-e377a391c7a2.jpeg')
INSERT [dbo].[Clothes] ([ClothID], [CategoryID], [TypeID], [CompanyID], [Price], [Discount], [Image]) VALUES (23, 2, 1, 5, 800.0000, 35, N'3e938435-44f0-430c-99bd-188c31e1f6fc.jpeg')
INSERT [dbo].[Clothes] ([ClothID], [CategoryID], [TypeID], [CompanyID], [Price], [Discount], [Image]) VALUES (25, 2, 1, 4, 679.0000, 25, N'cd057740-a7f5-418c-bb14-cacc53991b5b.jpeg')
INSERT [dbo].[Clothes] ([ClothID], [CategoryID], [TypeID], [CompanyID], [Price], [Discount], [Image]) VALUES (29, 2, 1, 4, 750.0000, 0, N'2909aeab-fe56-4b36-9a79-661d9043c0fa.jpeg')
INSERT [dbo].[Clothes] ([ClothID], [CategoryID], [TypeID], [CompanyID], [Price], [Discount], [Image]) VALUES (31, 2, 1, 1, 657.0000, 5, N'7e51f2d6-d2b7-46e2-9cbc-89b6485a8366.jpeg')
INSERT [dbo].[Clothes] ([ClothID], [CategoryID], [TypeID], [CompanyID], [Price], [Discount], [Image]) VALUES (32, 2, 1, 4, 980.0000, 0, N'd85660e0-dda2-42dc-86fc-acc9063bf9e2.jpeg')
INSERT [dbo].[Clothes] ([ClothID], [CategoryID], [TypeID], [CompanyID], [Price], [Discount], [Image]) VALUES (33, 2, 1, 1, 560.0000, 0, N'39415f6e-12d6-4453-88d0-1c3801904494.jpeg')
INSERT [dbo].[Clothes] ([ClothID], [CategoryID], [TypeID], [CompanyID], [Price], [Discount], [Image]) VALUES (35, 2, 1, 4, 900.0000, 0, N'da6bf0e7-f5d1-4c2f-bbd7-3a37bb92e43d.jpeg')
INSERT [dbo].[Clothes] ([ClothID], [CategoryID], [TypeID], [CompanyID], [Price], [Discount], [Image]) VALUES (36, 2, 5, 4, 678.0000, 0, N'c1ae9163-0a4a-4b4c-881b-909064075f2d.jpeg')
INSERT [dbo].[Clothes] ([ClothID], [CategoryID], [TypeID], [CompanyID], [Price], [Discount], [Image]) VALUES (37, 3, 4, 3, 670.0000, 0, N'c156d427-48bd-4762-8715-4e3b837b8172.jpeg')
INSERT [dbo].[Clothes] ([ClothID], [CategoryID], [TypeID], [CompanyID], [Price], [Discount], [Image]) VALUES (38, 1, 15, 5, 145.0000, 0, N'5cd146cc-d409-4cdb-af37-f9f0463afb00.jpeg')
INSERT [dbo].[Clothes] ([ClothID], [CategoryID], [TypeID], [CompanyID], [Price], [Discount], [Image]) VALUES (39, 3, 1, 4, 200.0000, 0, N'd4bdfb89-45b6-439d-9f9b-a83897368fc1.jpeg')
INSERT [dbo].[Clothes] ([ClothID], [CategoryID], [TypeID], [CompanyID], [Price], [Discount], [Image]) VALUES (40, 4, 1, 1, 300.0000, 0, N'8743141a-2bf1-4d4b-95b5-adab826d15d1.jpeg')
INSERT [dbo].[Clothes] ([ClothID], [CategoryID], [TypeID], [CompanyID], [Price], [Discount], [Image]) VALUES (41, 1, 1, 1, 230.0000, 0, N'55bf1589-56e3-4563-97d7-6da26a134a17.jpeg')
INSERT [dbo].[Clothes] ([ClothID], [CategoryID], [TypeID], [CompanyID], [Price], [Discount], [Image]) VALUES (42, 4, 1, 4, 120.0000, 4, N'1ce5f04e-750e-4df8-a573-d1936045130d.jpeg')
INSERT [dbo].[Clothes] ([ClothID], [CategoryID], [TypeID], [CompanyID], [Price], [Discount], [Image]) VALUES (43, 3, 18, 1, 50.0000, 5, N'8d6706e6-7043-403a-9a86-39d97907845c.jpg')
SET IDENTITY_INSERT [dbo].[Clothes] OFF
SET IDENTITY_INSERT [dbo].[Companies] ON 

INSERT [dbo].[Companies] ([CompanyID], [CompanyName]) VALUES (1, N'זארה')
INSERT [dbo].[Companies] ([CompanyID], [CompanyName]) VALUES (2, N'קסטרו')
INSERT [dbo].[Companies] ([CompanyID], [CompanyName]) VALUES (3, N'חנהלה ושמלת השבת')
INSERT [dbo].[Companies] ([CompanyID], [CompanyName]) VALUES (4, N'נאוטיקה')
INSERT [dbo].[Companies] ([CompanyID], [CompanyName]) VALUES (5, N'פרטי')
INSERT [dbo].[Companies] ([CompanyID], [CompanyName]) VALUES (6, N'קנדי')
SET IDENTITY_INSERT [dbo].[Companies] OFF
SET IDENTITY_INSERT [dbo].[Types] ON 

INSERT [dbo].[Types] ([TypeID], [TypeName]) VALUES (1, N'חולצה')
INSERT [dbo].[Types] ([TypeID], [TypeName]) VALUES (2, N'חגורה')
INSERT [dbo].[Types] ([TypeID], [TypeName]) VALUES (3, N'סרפן')
INSERT [dbo].[Types] ([TypeID], [TypeName]) VALUES (4, N'שמלה')
INSERT [dbo].[Types] ([TypeID], [TypeName]) VALUES (5, N'מכנס')
INSERT [dbo].[Types] ([TypeID], [TypeName]) VALUES (6, N'חליפה')
INSERT [dbo].[Types] ([TypeID], [TypeName]) VALUES (7, N'חצאית')
INSERT [dbo].[Types] ([TypeID], [TypeName]) VALUES (10, N'עניבה')
INSERT [dbo].[Types] ([TypeID], [TypeName]) VALUES (11, N'בגד גוף')
INSERT [dbo].[Types] ([TypeID], [TypeName]) VALUES (12, N'נעליים')
INSERT [dbo].[Types] ([TypeID], [TypeName]) VALUES (13, N'תיק')
INSERT [dbo].[Types] ([TypeID], [TypeName]) VALUES (14, N'שעון')
INSERT [dbo].[Types] ([TypeID], [TypeName]) VALUES (15, N'גופיה')
INSERT [dbo].[Types] ([TypeID], [TypeName]) VALUES (16, N'מעיל')
INSERT [dbo].[Types] ([TypeID], [TypeName]) VALUES (17, N'סריג')
INSERT [dbo].[Types] ([TypeID], [TypeName]) VALUES (18, N'סרט')
SET IDENTITY_INSERT [dbo].[Types] OFF
ALTER TABLE [dbo].[Clothes]  WITH CHECK ADD  CONSTRAINT [FK_Clothes_Categories] FOREIGN KEY([CategoryID])
REFERENCES [dbo].[Categories] ([CategoryID])
GO
ALTER TABLE [dbo].[Clothes] CHECK CONSTRAINT [FK_Clothes_Categories]
GO
ALTER TABLE [dbo].[Clothes]  WITH CHECK ADD  CONSTRAINT [FK_Clothes_Companies] FOREIGN KEY([CompanyID])
REFERENCES [dbo].[Companies] ([CompanyID])
GO
ALTER TABLE [dbo].[Clothes] CHECK CONSTRAINT [FK_Clothes_Companies]
GO
ALTER TABLE [dbo].[Clothes]  WITH CHECK ADD  CONSTRAINT [FK_Clothes_Types] FOREIGN KEY([TypeID])
REFERENCES [dbo].[Types] ([TypeID])
GO
ALTER TABLE [dbo].[Clothes] CHECK CONSTRAINT [FK_Clothes_Types]
GO
USE [master]
GO
ALTER DATABASE [ClothingStore] SET  READ_WRITE 
GO
