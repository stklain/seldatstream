﻿$(function () {
    $.ajax({
        method: "GET",//The Verb
        url: "http://localhost:61895/api/categories",
        cache: false,
        error: function (err) {
            alert(err.status + "," + err.statusText);
        },
        success: function (response) {
            for (var i = 0; i < response.length; i++) {
                var option = '<option value="' + response[i].id + '">' + response[i].name + '</option>';
                $("#select").append(option);
            }
        }
    });

    if (sessionStorage.getItem('Admin') == 'true') {
        var add = "<input type='button' value='Add' class='addCloth'/>";
        $('#add').html(add);
        var button = "<input type='button' value='Logout' class='logout'/>";
        $('#logout').html(button);
    }

    $('#login').prop('disabled', true);

    $('#username,#userpassword').keyup(function () {
        if ($("#userName").val() != '' && $("#userPassword").val() != '') {
            $('#login').prop('disabled', false);
        }
        else
            $('#login').prop('disabled', true);
    });

    $("#add").on('click', '.addCloth', function () {
        cleanDialog();
        $("#dialogEdit").dialog();
    });

    $("#loginDialog").click(function () {
        $("#dialogLogin").dialog();
    });

    function showUpdateDetails(id) {
        $('#dialogEdit').data('flag', "update");
        $.ajax({
            method: "GET",
            url: "http://localhost:61895/api/clothing/" + id,
            cache: false,
            error: function (err) {
                alert(err.status + ", " + err.statusText);
            },
            success: function (response) {
                $('#typeEdit').val(response.type.id);
                $('#companyEdit').val(response.company.id);
                $('#categoryEdit').val(response.category.id);
                $('#discountEdit').val(response.discount);
                $('#priceEdit').val(response.price);
                $("#saveCloth").removeClass();
                $("#saveCloth").addClass(id);
                $('#saveCloth').attr("data-pic", response.image);
                $('#dialogEdit').dialog();
            }
        });
    }

    $("#container").on('click', '.update', function () {
        cleanDialog();
        var id = $(this).attr('id');
        showUpdateDetails(id);
    });

    function cleanDialog() {
        $('#typeEdit').val(1);
        $('#companyEdit').val(1);
        $('#categoryEdit').val(1);
        $('#discountEdit').val("");
        $('#priceEdit').val("");
        $('#editedCloth').empty();
        $('#dialogEdit').data('flag', null);
    }

    $("#container").on('click', '.image', function () {
        $.ajax({
            method: "GET",
            url: "http://localhost:61895/api/clothing/" + $(this).attr('id'),
            cache: false,
            error: function (err) {
                alert("Error: " + err.status + "," + err.statusText);
            },
            success: function (response) {
                if (response.discount > 0) {
                    var newprice = parseInt(response.price - response.discount * response.price / 100);
                    var price = '<p class="price"><del>' + response.price + '</del></p>' + '<p class="newprice" style="color:red">' + newprice + '</p>';
                }
                else
                    var price = '<p class="price">' + response.price + '</p>';
                var div = '<div class= ' + response.id + '>' + '<p class="category">' + response.category.name + '</p>' + '<p class="company">' + response.company.name + '</p>' + '<p class="category">' + response.type.name + '</p>' + price + "<img src='http://localhost:61895/Images/" + response.image + "'/>" + '</div>';
                $("#dialogDetails").html(div);
                $("#dialogDetails").dialog();
            }
        });
    });

    $("#logout").on('click', '.logout', function () {
        sessionStorage.removeItem('Admin');
        window.open("index.html");
    });

    $("#container").on('click', '.delete', function () {
        $.ajax({
            method: "DELETE",
            url: "http://localhost:61895/api/clothing/" + $(this).attr('id'),
            cache: false,
            error: function (err) {
                alert(err.status + "," + err.statusText);
            },
            success: function () {
                alert("The DELETE Success");
            }
        });
    });

    $("#select").change(function () {
        $.ajax({
            method: "GET",
            url: "http://localhost:61895/api/categories/" + $(this).val(),
            cache: false,
            error: function (err) {
                alert("Error: " + err.status + "," + err.statusText);
            },
            success: function (response) {
                $("#container").empty();
                for (var i = 0; i < response.length; i++) {
                    var div = "<div id='image'><img class='image' id='" + response[i].id + "' src='http://localhost:61895/Images/" + response[i].image + "'/>";
                    if (sessionStorage.getItem('Admin') == 'true') {
                        div += "<br/><input type='button' value='delete' id='" + response[i].id + "'class='delete'><input type='button' value='update' id='" + response[i].id + "'class='update'>";

                    }
                    div += "</div>";
                    $("#container").append(div);
                }
            }
        });
    });

    $("#login").click(function () {
        $.ajax({
            method: "POST",
            url: "http://localhost:61895/api/admin",
            data: {
                name: $("#username").val(),
                password: $("#userpassword").val()
            },
            cache: false,
            error: function (err) {
                var errors = err.responseJSON;
                var error = "";
                for (var item in errors) {
                    for (var i = 0; i < errors[item].length; i++) {
                        error += errors[item][i];
                        error += "\n";
                    }
                }
                alert(error);
            },
            success: function (response) {
                sessionStorage.setItem('Admin', response);
                if (sessionStorage.getItem('Admin') == 'true')
                    window.open("index.html");
                else
                    alert("User in not exist");
            }
        });
    });
});
