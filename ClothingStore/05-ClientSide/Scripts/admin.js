﻿$(function () {
    $.ajax({
        method: "GET",
        url: "http://localhost:61895/api/categories",
        cache: false,
        error: function (err) {
            alert(err.status + "," + err.statusText);
        },
        success: function (response) {
            for (var i = 0; i < response.length; i++) {
                var option = '<option value="' + response[i].id + '">' + response[i].name + '</option>';
                $("#categoryEdit").append(option);
            }
        }
    });

    $.ajax({
        method: "GET",
        url: "http://localhost:61895/api/companies",
        cache: false,
        error: function (err) {
            alert(err.status + "," + err.statusText);
        },
        success: function (response) {
            for (var i = 0; i < response.length; i++) {
                var option = '<option value="' + response[i].id + '">' + response[i].name + '</option>';
                $("#companyEdit").append(option);
            }
        }
    });

    $.ajax({
        method: "GET",
        url: "http://localhost:61895/api/types",
        cache: false,
        error: function (err) {
            alert(err.status + "," + err.statusText);
        },
        success: function (response) {
            for (var i = 0; i < response.length; i++) {
                var option = '<option value="' + response[i].id + '">' + response[i].name + '</option>';
                $("#typeEdit").append(option);
            }
        }
    });

    $("#saveCloth").click(function () {
        if ($('#priceEdit').val()=="")
            alert("Missing Price");
        if ($('#discountEdit').val() < 0 || $('#discountEdit').val() > 100 && $('#discountEdit').val()!="")
            alert("Discont can't be negative and can't be large than 100");
        var formData = new FormData();
        formData.append("categoryIDEdit", $("#categoryEdit option:selected").val());
        formData.append("categoryNameEdit", $("#categoryEdit option:selected").text());
        formData.append("companyIDEdit", $("#companyEdit option:selected").val());
        formData.append("companyNameEdit", $("#companyEdit option:selected").text());
        formData.append("typeIDEdit", $("#typeEdit option:selected").val());
        formData.append("typeNameEdit", $("#typeEdit option:selected").text());
        formData.append("priceEdit", $("#priceEdit").val());
        formData.append("discountEdit", $("#discountEdit").val());
        var files = document.getElementById("imageEdit").files;
        if ($('#dialogEdit').data('flag') == null) {
            if (files.length > 0) {
                formData.append("imageEdit", files[0]);
            }
            else
            {
                alert("missing image");
            }
            $.ajax({
                type: "POST",
                url: "http://localhost:61895/api/clothing",
                data: formData,
                error: function (err) {
                    var errors = "";
                    for (var i in err.responseJSON) {
                        errors += err.responseJSON[i[0]];
                    }
                    alert(errors);
                },
                success: function (response) {
                    if (response.discount > 0) {
                        var newprice = parseInt(response.price - response.discount * response.price / 100);
                        var price = '<p class="price"><del>' + response.price + '</del></p>' + '<p class="newprice" style="color:red">' + newprice + '</p>';
                    }
                    else
                        var price = '<p class="price">' + response.price + '</p>';
                    var div = '<div class= ' + response.id + '>' + '<p class="category">' + response.category.name + '</p>' + '<p class="company">' + response.company.name + '</p>' + '<p class="category">' + response.type.name + '</p>' + price + "<img src='http://localhost:61895/Images/" + response.image + "'/>" + '</div>';
                    $("#editedCloth").html(div);

                },
                contentType: false,
                processData: false
            });
        }
        else {
            if (files.length > 0) {
                formData.append("imageEdit", files[0]);
            }
            else {
                var image = $('#saveCloth').attr('data-pic');
                formData.append("imageEdit", image);
            }

            var id = $("#saveCloth").attr('class');
            $.ajax({
                type: "PUT",
                url: "http://localhost:61895/api/clothing/" + id,
                data: formData,
                error: function (err) {
                    var errors = "";
                    for (var i in err.responseJSON) {
                        errors += err.responseJSON[i[0]];
                    }
                    alert(errors);
                },
                success: function (response) {
                    if (response.discount > 0) {
                        var newprice = parseInt(response.price - response.discount * response.price / 100);
                        var price = '<p class="price"><del>' + response.price + '</del></p>' + '<p class="newprice" style="color:red">' + newprice + '</p>';
                    }
                    else
                        var price = '<p class="price">' + response.price + '</p>';
                    var div = '<div class= ' + response.id + '>' + '<p class="category">' + response.category.name + '</p>' + '<p class="company">' + response.company.name + '</p>' + '<p class="category">' + response.type.name + '</p>' + price + "<img src='http://localhost:61895/Images/" + response.image + "'/>" + '</div>';
                    $("#editedCloth").html(div);
                },
                contentType: false,
                processData: false
            });
        }
    });
});