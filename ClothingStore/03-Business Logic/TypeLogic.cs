﻿
using System.Collections.Generic;
using System.Linq;

namespace Seldat {
    public class TypeLogic : BaseLogic {
        public List<TypeModel> GetAllTypes() {
            return DB.Types.Select(t => new TypeModel {
                id = t.TypeID,
                name = t.TypeName
            }).ToList();
        }


        public List<TypeModel> GetTypeByCategory(int categoryID) {
            return DB.Categories.Where(c => c.CategoryID == categoryID)
                .FirstOrDefault().Types
                .Select(t => new TypeModel {
                    id = t.TypeID,
                    name = t.TypeName
                }).ToList();
        }
    }
}

